def plot_scatter(points, scalars=None, notebook=False, opacity=1.0, rng=None, cmap=None):
    vertices = points.copy()

    plotter = pv.Plotter() if not notebook else pv.BackgroundPlotter()
    plotter.add_points(vertices,
                       point_size=10.0,
                       scalars=scalars,
                       opacity=opacity,
                       cmap=cmap, rng=rng)

    if scalars is not None:
        plotter.add_scalar_bar()

    plotter.show_grid()
    plotter.plot() if not notebook else plotter
