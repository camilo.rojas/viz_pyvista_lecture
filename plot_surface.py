def plot_surface(points, faces, scalars=None, notebook=False):
    n_faces = faces.shape[0]
    if scalars is None:
        scalars = np.arange(n_faces)

    surf = create_polydata(points, faces)
    plotter = pv.Plotter() if not notebook else pv.BackgroundPlotter()
    plotter.add_mesh(surf, scalars=scalars, show_edges=True, stitle='Colors', opacity=0.9)

    plotter.show_grid()
    plotter.plot() if not notebook else plotter
