
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import kneighbors_graph


def mse(vec1, vec2):
    return (np.square(vec1 - vec2)).mean()


def neighbor_measure(points):
    graph = kneighbors_graph(points, 26, mode='connectivity', include_self=False)
    neigh_list = []
    for p_index, row in enumerate(graph):
        nei_indexes = np.where(row.T.todense() == 1.0)[0]
#         print(points[nei_indexes].shape)
#         print(points[p_index].shape)
#         print((points[nei_indexes] - points[p_index]).shape)
        neigh_prom = np.linalg.norm(points[nei_indexes] - points[p_index], axis=1).sum()/26.0
#         print(neigh_prom)
        neigh_list.append(neigh_prom)
    return np.array(neigh_list)


def create_sub_structures(points, triangles):
    all_points = points.copy()
    all_triangles = triangles.copy()

    flat_triangles = np.unique(all_triangles.flatten()).tolist()

    sub_points = np.empty((len(flat_triangles), all_points.shape[1]))

    points_map = {}
    for i, val in enumerate(flat_triangles):
        sub_points[i] = all_points[flat_triangles[i]]
        points_map.update({flat_triangles[i]: i})

    sub_triangles = np.empty(all_triangles.shape)
    for i, tr in enumerate(all_triangles):
        sub_triangles[i] = np.array([points_map[val] for val in tr])

    return sub_points, sub_triangles.astype(int), points_map


def read_off(file_name):
    f = open(file_name, 'r')
    # First line is just a OFF string
    f.readline()
    # Second line has points and faces
    n_points, n_faces, dummy= [int(v) for v in f.readline().split(" ")]
    # print(n_points, n_faces)
    # Parse points in surface
    points = []
    for i in range(n_points):
        points.append([float(v) for v in f.readline().split(" ")])
    # Parse faces in surface
    faces = []
    for j in range(n_faces):
        faces.append([int(v) for v in f.readline().split(" ")][1:4])
    f.close()
    return np.array(points), np.array(faces)


def read_block_model(file_name, scale=True):
    bm_df = pd.read_csv(file_name)
    if bm_df.columns.contains('centroid_x'):
        bm_verts = bm_df[["centroid_x", "centroid_y", "centroid_z"]].values
    else:
        bm_verts = bm_df[["x", "y", "z"]].values
    
    if(scale):
        scaler = MinMaxScaler(feature_range=(0, 1))
        bm_verts_sc = scaler.fit_transform(bm_verts.reshape(bm_verts.size, 1)).reshape(bm_verts.shape)
    else:
        bm_verts_sc = bm_verts
    return bm_verts_sc


def write_off(verts, file_name):
    # export points
    f = open(file_name, "w")

    n_points = verts.shape[0]
    first_line = "%d\n" % (n_points)
    f.write(first_line)

    for i in range(n_points):
        line = "%.20f %.20f %.20f\n" % (verts[i][0], verts[i][1], verts[i][2])
        f.write(line)
    f.close()


def scale(data_vector):
    scaler = MinMaxScaler(feature_range=(0, 1))
    return scaler.fit_transform(data_vector.reshape((-1, 1)))
