def plot_tetras(points, simplices, opacity=0.3, notebook=False):
    # mesh points
    vertices = points.copy()

    # mesh faces
    faces = simplices.copy()

    # convert to vtki unstructered grid
    nvertices = vertices.shape[0]
    nfaces = faces.shape[0]
    # offset array.  Identifies the start of each cell in the cells array
    offset = np.array([i * 5 for i in range(0, nfaces)])

    # composing the cell
    face_types = (np.ones(faces.shape[0]) * 4).reshape((nfaces, 1)).astype(int)
    cells = np.hstack(np.append(face_types, faces, axis=1))

    cell_type = np.array([vtk.VTK_TETRA] * nfaces)

    # create the unstructured grid directly from the numpy arrays
    grid = pv.UnstructuredGrid(offset, cells, cell_type, vertices)

    plotter = pv.Plotter() if not notebook else pv.BackgroundPlotter()
    plotter.add_mesh(grid,
                     scalars=np.arange(nfaces),
                     stitle='Colors',
                     show_edges=True,
                     opacity=opacity)

    plotter.show_grid()
    plotter.plot() if not notebook else plotter
