
import pyvista as pv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
import vtk


def create_polydata(points, faces):
    # mesh points
    vertices = points

    # mesh faces
    n_faces = faces.shape[0]
    face_types = (np.ones(faces.shape[0]) * 3).reshape((n_faces, 1)).astype(int)
    faces = np.append(face_types, faces, axis=1)

    surf = pv.PolyData(vertices, faces)
    return surf


def plot_surface(points, faces, scalars=None, notebook=False):
    n_faces = faces.shape[0]
    if scalars is None:
        scalars = np.arange(n_faces)

    surf = create_polydata(points, faces)
    plotter = pv.Plotter() if not notebook else pv.BackgroundPlotter()
    plotter.add_mesh(surf, scalars=scalars, show_edges=True, stitle='Colors', opacity=0.9)

    plotter.show_grid()
    plotter.plot() if not notebook else plotter


def plot_tetras(points, simplices, opacity=0.3, notebook=False):
    # mesh points
    vertices = points.copy()

    # mesh faces
    faces = simplices.copy()

    # convert to vtki unstructered grid
    nvertices = vertices.shape[0]
    nfaces = faces.shape[0]
    # offset array.  Identifies the start of each cell in the cells array
    offset = np.array([i * 5 for i in range(0, nfaces)])

    # composing the cell
    face_types = (np.ones(faces.shape[0]) * 4).reshape((nfaces, 1)).astype(int)
    cells = np.hstack(np.append(face_types, faces, axis=1))

    cell_type = np.array([vtk.VTK_TETRA] * nfaces)

    # create the unstructured grid directly from the numpy arrays
    grid = pv.UnstructuredGrid(offset, cells, cell_type, vertices)

    plotter = pv.Plotter() if not notebook else pv.BackgroundPlotter()
    plotter.add_mesh(grid,
                     scalars=np.arange(nfaces),
                     stitle='Colors',
                     show_edges=True,
                     opacity=opacity)

    plotter.show_grid()
    plotter.plot() if not notebook else plotter


def plot_scatter(points, scalars=None, notebook=False, opacity=1.0, rng=None, cmap=None):
    vertices = points.copy()

    plotter = pv.Plotter() if not notebook else pv.BackgroundPlotter()
    plotter.add_points(vertices,
                       point_size=10.0,
                       scalars=scalars,
                       opacity=opacity,
                       cmap=cmap, rng=rng)

    if scalars is not None:
        plotter.add_scalar_bar()

    plotter.show_grid()
    plotter.plot() if not notebook else plotter


def plot_surface_and_points(points, faces, all_points, scalars=None):
    n_faces = faces.shape[0]
    if (scalars is None):
        scalars = np.arange(n_faces)

    surf = create_polydata(points, faces)
    plotter = pv.Plotter()
    plotter.add_mesh(surf, scalars=scalars, show_edges=True, stitle='Colors', opacity=0.4)
    plotter.add_points(all_points, point_size=3.0)
    plotter.show_grid()
    plotter.show()


def plot_two_surfaces(points1, faces1, points2, faces2, all_points=None, scalars=None, opacity=0.5):
    n_faces1 = faces1.shape[0]
    scalars1 = np.arange(n_faces1)

    n_faces2 = faces2.shape[0]
    scalars2 = np.arange(n_faces2)

    surf1 = create_polydata(points1, faces1)
    surf2 = create_polydata(points2, faces2)
    plotter = pv.Plotter()
    plotter.add_mesh(surf1, scalars=scalars1, opacity=opacity, show_edges=True, stitle='Colors')
    # plotter.add_points(points1, point_size=5.0, scalars=scalars)

    plotter.add_mesh(surf2, scalars=scalars2, opacity=opacity, show_edges=True, stitle='Colors')
    # plotter.add_points(points2, point_size=5.0, scalars=scalars)

    if all_points is not None:
        plotter.add_points(all_points)

    plotter.show_grid()
    plotter.show()


def plot_triangulation(points, triangles, scalars=None, title=''):
    plt.tripcolor(points[:, 0], points[:, 1], triangles, scalars)
    plt.colorbar()
    plt.title(title)
    plt.show()


def plot_mse(mse, areas=None, title=''):
    fig, ax1 = plt.subplots()
    its = np.array([i+1for i in range(mse.shape[0])])
    # color = 'tab:darkGreen'
    ax1.plot(its, mse, color='C2')
    ax1.set_xlabel('Iterations')
    ax1.set_ylabel('Mean Square Error', color='C2')
    ax1.tick_params(axis='y', labelcolor='C2')

    if areas is not None:
        ax2 = ax1.twinx()
        ax2.set_ylabel('Flat Area / Surface Area', color='C0')
        ax2.plot(its, areas, color='C0')
        ax2.tick_params(axis='y', labelcolor='C0')
    fig.tight_layout()
    plt.title(title)
    # plt.grid(True)
    plt.show()


def plot_triangles_diff(orig_verts, flat_verts, triangles):
    one_triangle = triangles[0]

    one_triangle_pts_flt = np.array([flat_verts[ind, :] for ind in one_triangle])
    x_flt = one_triangle_pts_flt[:, 0]
    y_flt = one_triangle_pts_flt[:, 1]
    triang_flt = Triangulation(x_flt, y_flt, triangles=[[0, 1, 2]])

    original_verts = np.array(orig_verts)
    one_triangle_pts = np.array([original_verts[ind, :] for ind in one_triangle])
    x = one_triangle_pts[:, 0]
    y = one_triangle_pts[:, 1]
    triang = Triangulation(x, y, triangles=[[0, 1, 2]])

    plt.figure()
    plt.gca().set_aspect('equal')
    line2 = plt.triplot(triang, 'bo-', lw=1)
    line = plt.triplot(triang_flt, 'rx--', lw=1)
    plt.title('One Triangle')
    print(line)
    plt.legend(line + line2, ['flat_triangle', 'flat_points', 'original_triangle', 'original_points'])
    plt.show()

def plot_hist(values, coords=(.1, 2000), bins=100):
    plt.hist(values, bins=bins)
    plt.xlabel('Error Values')
    plt.ylabel('Frequency')
    plt.title('Frequency Error')
    plt.grid(True)
    plt.text(coords[0], coords[1], r'$\mu={0:6f},\ \sigma={0:6f}$'.format(np.mean(values), np.std(values)))
    plt.show()


def plot_cumulative(values, bins=100):
    plt.hist(values, bins=bins, cumulative=True, histtype='step')
    plt.xlabel('Error Values')
    plt.ylabel('Total Samples')
    plt.title('Cumulative Error Distribution')
    plt.grid(True)
    plt.show()
